using Leopotam.Ecs;

namespace MageFortress
{
    [EcsInject]
    sealed class FortressSystem : IEcsRunSystem
    {
        // Auto-injected fields.
        EcsWorld world = null;

        void IEcsRunSystem.Run()
        {
            // Add your run code here.
        }
    }
}