﻿using System.Reflection;
using System.Text;
using UnityEngine;

namespace MageFortress
{
    public static class ComponentsDebugHelper
    {
        public static string GetString<T>(T component)
        {
            var stringBuilder = new StringBuilder(200);

            stringBuilder.Append(component.GetType().Name);

            var fieldsInfos = component.GetType().GetFields(BindingFlags.Public | BindingFlags.Instance);

            for (var i = 0; i < fieldsInfos.Length; i++)
            {
                var fieldType = fieldsInfos[i].FieldType;
                var fieldStringValue = string.Empty;
                if (fieldType == typeof(Vector2))
                {
                    fieldStringValue = ((Vector2)fieldsInfos[i].GetValue(component)).ToString("G4");
                }
                else if (fieldType == typeof(Vector3))
                {
                    fieldStringValue = ((Vector3)fieldsInfos[i].GetValue(component)).ToString("G4");
                }
                else if (fieldType == typeof(Vector4))
                {
                    fieldStringValue = ((Vector4)fieldsInfos[i].GetValue(component)).ToString("G4");
                }
                else
                {
                    fieldStringValue = fieldsInfos[i].GetValue(component).ToString();
                }

                stringBuilder.AppendFormat(" {0}: {1},", fieldsInfos[i].Name, fieldStringValue);
            }

            if (fieldsInfos.Length > 0)
            {
                stringBuilder.Remove(stringBuilder.Length - 1, 1);
            }

            return stringBuilder.ToString();
        }
    }
}
