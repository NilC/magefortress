using Leopotam.Ecs;
using UnityEngine;

namespace MageFortress
{
    [EcsInject]
    sealed class TestSystem : IEcsRunSystem
    { 
        EcsWorld world = null;

        EcsFilter<EnemyComponent> enemies = null;

        void IEcsRunSystem.Run()
        {
            if (Input.GetKeyUp(KeyCode.T))
            {
                for (int i = 0, count = enemies.EntitiesCount; i < count; i++)
                {
                    var damageHealthMessage = world.CreateEntityWith<DamageHealthMessage>();
                    damageHealthMessage.EntityId = enemies.Entities[i];
                    damageHealthMessage.DamageValue = 10;
                    damageHealthMessage.IsCritical = false;
                }
            }
        }
    }
}
