namespace MageFortress
{
    sealed class HealthComponent
    {
        public int MaxHealth;
        //[ReadOnlyInInspector]
        public int Health;
        //[ReadOnlyInInspector]
        public float CurrentAccumulatedDamage;
        public bool IsImmortal;
        //[ReadOnlyInInspector]
        public bool IsDead;
    }
}