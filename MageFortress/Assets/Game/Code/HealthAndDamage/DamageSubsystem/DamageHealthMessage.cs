using Leopotam.Ecs;

namespace MageFortress
{
    [EcsOneFrame]
    sealed class DamageHealthMessage
    {
        public int EntityId;
        public int DamageValue;
        public bool IsCritical;

        public override string ToString() { return ComponentsDebugHelper.GetString(this); }
    }
}