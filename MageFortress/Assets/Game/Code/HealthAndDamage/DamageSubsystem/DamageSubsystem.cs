using Leopotam.Ecs;
using UnityEngine;

namespace MageFortress
{
    [EcsInject]
    sealed class DamageSubsystem : IEcsRunSystem
    {
        // Auto-injected fields.
        EcsWorld world = null;

        EcsFilter<DamageHealthMessage> damages = null;

        void IEcsRunSystem.Run()
        {
            for (int i = 0, count = damages.EntitiesCount; i < count; i++)
            {
                Debug.Log(damages.Components1[i].ToString());
                //var damageHealthMessage = world.CreateEntityWith<DamageHealthMessage>();
                //damageHealthMessage.EntityId = enemies.Entities[i];
                //damageHealthMessage.DamageValue = 10;
                //damageHealthMessage.IsCritical = false;
            }
        }
    }
}