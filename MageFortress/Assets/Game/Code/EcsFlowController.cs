using Leopotam.Ecs;
using UnityEngine;

namespace MageFortress
{
    sealed class EcsFlowController : MonoBehaviour
    {
        EcsWorld world;
        EcsSystems systems;

        void OnEnable()
        {
            world = new EcsWorld();
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(world);
#endif
            systems = new EcsSystems(world);
            systems
                .Add(new PreCreatedOnSceneRegistrationSystem())
                .Add(new GameObjectsFactory())

                //Tests
                .Add(new TestSystem())

                //Attack and Health
                .Add(new DamageSubsystem())
                .Add(new HealthSystem())

                .Initialize();
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(systems);
#endif
        }

        void Update()
        {
            systems.Run();
            // Optional: One-frame components cleanup.
            world.RemoveOneFrameComponents();
        }

        void OnDisable()
        {
            systems.Dispose();
            systems = null;
            world.Dispose();
            world = null;
        }
    }
}